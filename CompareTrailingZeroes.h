//
// Created by herman on 3/1/20.
//

#ifndef SPECIAL_SORT_COMPARETRAILINGZEROES_H
#define SPECIAL_SORT_COMPARETRAILINGZEROES_H


#include <functional>

size_t countTrailingZeroes(int x);

class LessTrailingZeroes : public std::binary_function<int, int, bool> {
public:
    bool operator()(int, int) const;
};

class GreaterTrailingZeroes : public std::binary_function<int, int, bool> {
public:
    bool operator()(int, int) const;
};

#endif //SPECIAL_SORT_COMPARETRAILINGZEROES_H
