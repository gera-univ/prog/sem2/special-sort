#include <fstream>
#include <vector>
#include <algorithm>

#include "CompareTrailingZeroes.h"

int main() {
    std::ifstream input("input.txt");
    std::ofstream output("output.txt");

    std::vector<int> data;

    if (!input.is_open() || input.peek() == std::ifstream::traits_type::eof()) {
        output << "Cannot open the input file or it is empty\n";
        return 1;
    }

    int number;
    while (input >> number) {
        data.emplace_back(number);
    }

    std::sort(data.begin(), data.end(), LessTrailingZeroes());

    for (int number : data)
        output << number << "\n";

    input.close();
    output.close();
    return 0;
}
