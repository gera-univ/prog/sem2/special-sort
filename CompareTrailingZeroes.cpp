//
// Created by herman on 3/1/20.
//

#include "CompareTrailingZeroes.h"

bool LessTrailingZeroes::operator()(const int x, const int y) const {
    return countTrailingZeroes(x) < countTrailingZeroes(y);
}

bool GreaterTrailingZeroes::operator()(const int x, const int y) const {
    return countTrailingZeroes(x) > countTrailingZeroes(y);
}

size_t countTrailingZeroes(int x) {
    size_t result = 0;
    while (x % 10 == 0) {
        ++result;
        if (x == 0)
            break;
        x /= 10;
    }
    return result;
}
